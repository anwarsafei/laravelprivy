**How To Setup Project:**
1. Place the project on your htdocs folder (if you using xampp)
2. Create database using name "privy"
3. Import the sql from \sql\privy.sql
4. Run this command in the project using cmd:
    composer install
    php artisan key:generate
5. Open this url in browser: laravelprivy/public/