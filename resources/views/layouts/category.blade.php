
@extends('layouts.app')


@section('content')
<section class="content-header">
      <h1>
        Anwar
        <small>it all starts here</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Examples</a></li>
        <li class="active">Blank page</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Category</h3>
        </div>
        <div class="box-body">
          <!-- Start creating your amazing application! -->

          <div id="form-name" class="form-group">
              <div class="col-xs-12 col-sm-12 col-md-3 col-lg-2 col-header">
                  <span class="label label-danger label-required">Required</span>
                  <strong class="field-title">Name</strong>
              </div>
              <div class="col-xs-12 col-sm-12 col-md-9 col-lg-10 col-content">
                  {{ Form::text('email', $category->name, array('placeholder' => ' ', 'class' => 'form-control validate[required, maxSize[255]]')) }}
              </div>
          </div>

          <div id="form-enable" class="form-group">
              <div class="col-xs-12 col-sm-12 col-md-3 col-lg-2 col-header">
                  <span class="label label-danger label-required">Required</span>
                  <strong class="field-title">Enable</strong>
              </div>
              <div class="col-xs-12 col-sm-12 col-md-9 col-lg-10 col-content">
                  {{ Form::checkbox('category',null, $category->enable) }}
              </div>
          </div>

        </div>
        <!-- /.box-body -->
        <!-- <div class="box-footer">
          Footer
        </div> -->
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
@endsection