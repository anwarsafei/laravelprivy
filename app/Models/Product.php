<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 12 Nov 2019 10:03:37 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Product
 * 
 * @property int $id
 * @property string $name
 * @property string $description
 * @property bool $enable
 * 
 * @property \Illuminate\Database\Eloquent\Collection $categories
 * @property \Illuminate\Database\Eloquent\Collection $images
 *
 * @package App\Models
 */
class Product extends Eloquent
{
	protected $table = 'product';
	public $timestamps = false;

	protected $casts = [
		'enable' => 'bool'
	];

	protected $fillable = [
		'name',
		'description',
		'enable'
	];

	public function categories()
	{
		return $this->belongsToMany(\App\Models\Category::class);
	}

	public function images()
	{
		return $this->belongsToMany(\App\Models\Image::class, 'product_image');
	}
}
