<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 12 Nov 2019 10:03:37 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class CategoryProduct
 * 
 * @property int $product_id
 * @property int $category_id
 * 
 * @property \App\Models\Category $category
 * @property \App\Models\Product $product
 *
 * @package App\Models
 */
class CategoryProduct extends Eloquent
{
	protected $table = 'category_product';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'product_id' => 'int',
		'category_id' => 'int'
	];

	protected $fillable = [
		'product_id',
		'category_id'
	];

	public function category()
	{
		return $this->belongsTo(\App\Models\Category::class);
	}

	public function product()
	{
		return $this->belongsTo(\App\Models\Product::class);
	}
}
