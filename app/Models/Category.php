<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 12 Nov 2019 10:03:37 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Category
 * 
 * @property int $id
 * @property string $name
 * @property bool $enable
 * 
 * @property \Illuminate\Database\Eloquent\Collection $products
 *
 * @package App\Models
 */
class Category extends Eloquent
{
	protected $table = 'category';
	public $timestamps = false;

	protected $casts = [
		'enable' => 'bool'
	];

	protected $fillable = [
		'name',
		'enable'
	];

	public function products()
	{
		return $this->belongsToMany(\App\Models\Product::class);
	}
}
