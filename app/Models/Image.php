<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 12 Nov 2019 10:03:37 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Image
 * 
 * @property int $id
 * @property string $name
 * @property string $file
 * @property bool $enable
 * 
 * @property \Illuminate\Database\Eloquent\Collection $products
 *
 * @package App\Models
 */
class Image extends Eloquent
{
	protected $table = 'image';
	public $timestamps = false;

	protected $casts = [
		'enable' => 'bool'
	];

	protected $fillable = [
		'name',
		'file',
		'enable'
	];

	public function products()
	{
		return $this->belongsToMany(\App\Models\Product::class, 'product_image');
	}
}
